<?php

namespace App\Test\Controller\Api;

class CategoryControllerTest extends AbstractApiControllerTest
{
    public function testListCategories()
    {
        $fixtures = $this->loadFixtures(['filter_test']);
        $this->client->request('GET', '/api/categories');
        $categories = $this->findInFixtures('/^category.+/', $fixtures);
        $expectedCategories = [];
        foreach ($categories as $category) {
            array_push($expectedCategories, $this->normalizer->normalize($category, null, ['groups' => 'list_categories']));
        }
        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals($expectedCategories, "Le tableau de categories attendu n'a pas été obtenu.");
    }
}
