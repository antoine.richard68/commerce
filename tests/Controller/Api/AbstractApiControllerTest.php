<?php

namespace App\Test\Controller\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Symfony\Component\Serializer\SerializerInterface;
use App\Tests\FixturesTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

abstract class AbstractApiControllerTest extends ApiTestCase
{
    use FixturesTrait;
    
    /** @var Client */
    protected $client;
    
    /** @var SerializerInterface */
    protected $serializer;
    
    /** @var NormalizerInterface */
    protected $normalizer;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->serializer = $this->getContainer()->get(SerializerInterface::class);
        $this->normalizer = $this->getContainer()->get(NormalizerInterface::class);
    }
}
