<?php

namespace App\Test\Controller\Api;

class ColorControllerTest extends AbstractApiControllerTest
{
    public function testListColors()
    {
        $fixtures = $this->loadFixtures(['filter_test']);
        $this->client->request('GET', '/api/colors');
        $colors = $this->findInFixtures('/^color.+/', $fixtures);
        $expectedColors = [];
        foreach ($colors as $color) {
            array_push($expectedColors, $this->normalizer->normalize($color, null, ['groups' => 'list_colors']));
        }
        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals($expectedColors, "Le tableau de couleurs attendu n'a pas été obtenu.");
    }
}
