<?php

namespace App\Test\Controller\Api;

use App\Model\Filter;
use App\Service\ProductService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;

class ProductControllerTest extends AbstractApiControllerTest
{
    public function testFilter()
    {
        $fixtures = $this->loadFixtures(['filter_test']);
        $filter = new Filter();
        $filter->categories = new ArrayCollection([]);
        $filter->price = ["min" => 0., "max" => 200.];
        $filter->grade = ['min' => 0., 'max' => 10.];
        
        $queryFilter = http_build_query($this->normalizer->normalize($filter));
        $this->client->request('GET', '/api/products?'.$queryFilter);
        
        $productService = $this->getContainer()->get(ProductService::class);
        $expectedData = $this->normalizer->normalize($productService->manageFilterRequest($filter), null, ['groups' => 'list_products']);
        
        $this->assertResponseIsSuccessful();
        $this->assertJsonEquals($expectedData, "Le tableau de produits attendu n'a pas été obtenu.");
        
    }
}
