<?php

namespace App\Tests\Service;

use App\Model\Filter;
use App\Service\ProductService;
use App\Tests\FixturesTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ProductServiceTest extends WebTestCase
{
    use FixturesTrait;
    
    /** @var ProductService $productService*/
    private $productService;
    
    public function setUp(): void
    {
        self::bootKernel();
        $this->productService = $this->getContainer()->get(ProductService::class);
    }
    
    public function testManageFilterRequest()
    {
        $fixtures = $this->loadFixtures(['filter_test']);
        
        $page = 2;
        $limit = 12;
        
        //count all products from fixtures except 'product1' which doesn't match the $filter below
        //in order to find the expected total of items and pages
        $expectedItems = count($this->findInFixtures('/^product(?![1]\b)\d+/', $fixtures));
        $expectedPages = ceil($expectedItems / $limit);
        
        $filter = new Filter();
        $filter->categories = new ArrayCollection([]);
        $filter->price = ["min" => 0, "max" => 100];
        $filter->grade = ['min' => 0, 'max' => 10];
        $filter->page = $page;
        $filter->limit = $limit;
        
        $data = $this->productService->manageFilterRequest($filter);
        $this->assertEquals($expectedItems, $data["results"]->count());
        $this->assertEquals($expectedItems, $data["pagination"]["totalItems"]);
        
        $this->assertEquals($page, $data["pagination"]["currentPage"]);
        
        $this->assertEquals($limit, $data["pagination"]["currentLimit"]);
        $this->assertEquals($limit, $data["results"]->getQuery()->getMaxResults());
        
        $this->assertEquals($expectedPages, $data["pagination"]["totalPages"]);
    }
}
