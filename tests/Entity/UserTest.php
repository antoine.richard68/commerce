<?php

namespace App\Tests\Entity;

use App\Entity\Order;
use App\Entity\Picture;
use App\Entity\Review;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $roles = ["ROLE_USER", "ROLE_ADMIN"];
        $reviews = [new Review(), new Review()];
        $orders = [new Order(), new Order()];
        $user = $this->createUser($roles, $orders, $reviews);
        
        $this->assertEquals($user->getEmail(), "truelle@mur.fr");
        $this->assertEquals($user->getUserIdentifier(), "truelle.mur");
        $this->assertEquals($user->getAddress(), "address");
        $this->assertEquals($user->getPassword(), "12345678");
        $this->assertEquals($user->getOrders()->toArray(), $orders);
        $this->assertEquals($user->getReviews()->toArray(), $reviews);
        $this->assertEquals($user->getRoles(), $roles);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $roles = ["ROLE_USER", "ROLE_ADMIN"];
        $reviews = [new Review(), new Review()];
        $orders = [new Order(), new Order()];
        $user = $this->createUser($roles, $orders, $reviews);
        
        $this->assertNotEquals($user->getEmail(), "false@assert.fr");
        $this->assertNotEquals($user->getAddress(), "false");
        $this->assertNotEquals($user->getUserIdentifier(), "false");
        $this->assertNotEquals($user->getPassword(), "false");
        $this->assertNotEquals($user->getOrders()->toArray(), [new Order()]);
        $this->assertNotEquals($user->getReviews()->toArray(), [new Review()]);
        $this->assertNotEquals($user->getRoles(), ["ROLE_USER"]);
    }
    
    public function testAddAndRemoveOrders(): void
    {
        $user = $this->createUser();
        $order = new Order();
        $this->assertEquals($user->getOrders()->toArray(), []);
        $user->addOrder($order);
        $this->assertEquals($user->getOrders()->toArray(), [$order]);
        $user->removeOrder($order);
        $this->assertEquals($user->getOrders()->toArray(), []);
    }
    
    public function testAddAndRemoveReviews(): void
    {
        $user = $this->createUser();
        $review = new Review();
        $this->assertEquals($user->getReviews()->toArray(), []);
        $user->addReview($review);
        $this->assertEquals($user->getReviews()->toArray(), [$review]);
        $user->removeReview($review);
        $this->assertEquals($user->getReviews()->toArray(), []);
    }
    
    
    /**
     * @param Picture $picture
     * @param string[] $roles
     * @param Order[] $orders
     * @param Review[] $reviews
     *
     * @return User
     */
    private function createUser(
        array $roles = array(),
        array $orders = array(),
        array $reviews = array()
    ): User {
        $user = (new User())
            ->setEmail("truelle@mur.fr")
            ->setUsername("truelle.mur")
            ->setAddress("address")
            ->setPassword("12345678")
            ->setRoles($roles);
        foreach ($orders as $order) {
            $user->addOrder($order);
        }
        foreach ($reviews as $review) {
            $user->addReview($review);
        }
        return $user;
    }
}
