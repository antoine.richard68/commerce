<?php

namespace App\Tests\Entity;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $customer = new User();
        $orderDetails = [new OrderDetail(), new OrderDetail()];
        $order = $this->createOrder($customer, $orderDetails);
        
        $this->assertEquals($order->getCustomer(), $customer);
        $this->assertEquals($order->getAddress(), "address");
        $this->assertEquals($order->getOrderDetails()->toArray(), $orderDetails);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $customer = new User();
        $orderDetails = [new OrderDetail(), new OrderDetail()];
        $order = $this->createOrder($customer, $orderDetails);
        
        $this->assertNotEquals($order->getCustomer(), (new User())->setUsername("false"));
        $this->assertNotEquals($order->getAddress(), "false");
        $this->assertNotEquals($order->getOrderDetails()->toArray(), [new OrderDetail()]);
    }
    
    public function testAddAndRemoveOrderDetails(): void
    {
        $order = $this->createOrder();
        $orderDetail = new OrderDetail();
        $this->assertEquals($order->getOrderDetails()->toArray(), []);
        $order->addOrderDetail($orderDetail);
        $this->assertEquals($order->getOrderDetails()->toArray(), [$orderDetail]);
        $order->removeOrderDetail($orderDetail);
        $this->assertEquals($order->getOrderDetails()->toArray(), []);
    }
     
    /**
     * @param User $customer
     * @param OrderDetail[] $orderDetails
     *
     * @return Order
     */
    private function createOrder(User $customer = null, array $orderDetails = array()): Order
    {
        $order = (new Order())
            ->setCustomer($customer)
            ->setAddress("address");
        foreach ($orderDetails as $orderDetail) {
            $order->addOrderDetail($orderDetail);
        }
        return $order;
    }
}
