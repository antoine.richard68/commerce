<?php

namespace App\Tests\Entity;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class OrderDetailTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $order = new Order();
        $product = new Product();
        $orderDetail = $this->createOrderDetail($order, $product);
        
        $this->assertEquals($orderDetail->getRelatedOrder(), $order);
        $this->assertEquals($orderDetail->getProduct(), $product);
        $this->assertEquals($orderDetail->getQuantity(), 5);
        $this->assertEquals($orderDetail->getDiscount(), 5.0);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $order = new Order();
        $product = new Product();
        $orderDetail = $this->createOrderDetail($order, $product);
        
        $this->assertNotEquals($orderDetail->getRelatedOrder(), "false");
        $this->assertNotEquals($orderDetail->getProduct(), (new Product())->setName("false"));
        $this->assertNotEquals($orderDetail->getQuantity(), 0);
        $this->assertNotEquals($orderDetail->getDiscount(), 0.0);
    }
    
    private function createOrderDetail(Order $order = null, Product $product = null): OrderDetail
    {
        return (new OrderDetail())
                ->setRelatedOrder($order)
                ->setProduct($product)
                ->setQuantity(5)
                ->setDiscount(5.0);
    }
}
