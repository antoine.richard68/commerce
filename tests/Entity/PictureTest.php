<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Picture;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class PictureTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $category = new Category();
        $product = new Product();
        $picture = $this->createPicture($category, $product);
        
        $this->assertEquals($picture->getTitle(), "title");
        $this->assertEquals($picture->getPath(), "path");
        $this->assertEquals($picture->getProduct(), $product);
        $this->assertEquals($picture->getCategory(), $category);
        $this->assertEquals($picture->getDescription(), "une description");
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $category = new Category();
        $product = new Product();
        $picture = $this->createPicture($category, $product);
        
        $this->assertNotEquals($picture->getTitle(), "false");
        $this->assertNotEquals($picture->getPath(), "false");
        $this->assertNotEquals($picture->getProduct(), (new Product())->setName("false"));
        $this->assertNotEquals($picture->getCategory(), (new Category())->setName("false"));
        $this->assertNotEquals($picture->getDescription(), "false");
    }
    
    private function createPicture(Category $category = null, Product $product = null): Picture
    {
        return (new Picture())
                ->setCategory($category)
                ->setProduct($product)
                ->setTitle("title")
                ->setPath("path")
                ->setDescription("une description");
    }
}
