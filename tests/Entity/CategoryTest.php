<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Picture;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $products = [new Product(), new Product()];
        $pictures = [new Picture(), new Picture()];
        $category = $this->createCategory($pictures, $products);
        
        $this->assertEquals($category->getName(), "category");
        $this->assertEquals($category->getSlug(), "category");
        $this->assertEquals($category->getFaIcon(), "fas fa-test");
        $this->assertEquals($category->getDescription(), "description");
        $this->assertEquals($category->getPictures()->toArray(), $pictures);
        $this->assertEquals($category->getProducts()->toArray(), $products);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $products = [new Product(), new Product()];
        $pictures = [new Picture(), new Picture()];
        $category = $this->createCategory($pictures, $products);
        
        $this->assertNotEquals($category->getName(), "false");
        $this->assertNotEquals($category->getSlug(), "false");
        $this->assertNotEquals($category->getFaIcon(), "fas fa-false");
        $this->assertNotEquals($category->getDescription(), "false");
        $this->assertNotEquals($category->getPictures()->toArray(), [new Picture()]);
        $this->assertNotEquals($category->getProducts()->toArray(), [new Product()]);
    }
    
    public function testAddAndRemovePictures(): void
    {
        $category = $this->createCategory();
        $picture = new Picture();
        $this->assertEquals($category->getPictures()->toArray(), []);
        $category->addPicture($picture);
        $this->assertEquals($category->getPictures()->toArray(), [$picture]);
        $category->removePicture($picture);
        $this->assertEquals($category->getPictures()->toArray(), []);
    }
    
    public function testAddAndRemoveProducts(): void
    {
        $category = $this->createCategory();
        $review = new Product();
        $this->assertEquals($category->getProducts()->toArray(), []);
        $category->addProduct($review);
        $this->assertEquals($category->getProducts()->toArray(), [$review]);
        $category->removeProduct($review);
        $this->assertEquals($category->getProducts()->toArray(), []);
    }
    
    
    
    /**
     * @param Picture[] $pictures
     * @param Product[] $products
     *
     * @return Category
     */
    private function createCategory(array $pictures = array(), array $products = array()): Category
    {
        $category = (new Category())
            ->setName("category")
            ->setSlug("category")
            ->setDescription("description")
            ->setFaIcon("fas fa-test");
        foreach ($pictures as $picture) {
            $category->addPicture($picture);
        }
        foreach ($products as $review) {
            $category->addProduct($review);
        }
        return $category;
    }
}
