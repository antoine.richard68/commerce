<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Color;
use App\Entity\OrderDetail;
use App\Entity\Picture;
use App\Entity\Product;
use App\Entity\Review;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $category = new Category();
        $tags = ["vintage", "mode"];
        $reviews = [new Review(), new Review()];
        $orderDetails = [new OrderDetail(), new OrderDetail()];
        $pictures = [new Picture(), new Picture()];
        $colors = [new Color(), new Color()];
        $createdAt = new \DateTimeImmutable("now");
        $product = $this->createProduct($category, $tags, $orderDetails, $reviews, $pictures, $colors, $createdAt);
        
        $this->assertEquals($product->getName(), "name");
        $this->assertEquals($product->getSlug(), "slug");
        $this->assertEquals($product->getDescription(), "description");
        $this->assertEquals($product->getStock(), 100);
        $this->assertEquals($product->getPrice(), 15.50);
        $this->assertEquals($product->getCategory(), $category);
        $this->assertEquals($product->getOrderDetails()->toArray(), $orderDetails);
        $this->assertEquals($product->getPictures()->toArray(), $pictures);
        $this->assertEquals($product->getReviews()->toArray(), $reviews);
        $this->assertEquals($product->getColors()->toArray(), $colors);
        $this->assertEquals($product->getTags(), $tags);
        $this->assertEquals($product->getCreatedAt(), $createdAt);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $category = new Category();
        $tags = ["vintage", "mode"];
        $reviews = [new Review(), new Review()];
        $orderDetails = [new OrderDetail(), new OrderDetail()];
        $pictures = [new Picture(), new Picture()];
        $colors = [new Color(), new Color()];
        $createdAt = new \DateTimeImmutable("now");
        $product = $this->createProduct($category, $tags, $orderDetails, $reviews, $pictures, $colors, $createdAt);
                
        $this->assertNotEquals($product->getName(), "false");
        $this->assertNotEquals($product->getSlug(), "false");
        $this->assertNotEquals($product->getDescription(), "false");
        $this->assertNotEquals($product->getStock(), 0);
        $this->assertNotEquals($product->getPrice(), 0.50);
        $this->assertNotEquals($product->getCategory(), (new Category)->setName("false"));
        $this->assertNotEquals($product->getOrderDetails()->toArray(), [new OrderDetail()]);
        $this->assertNotEquals($product->getPictures()->toArray(), [new Picture()]);
        $this->assertNotEquals($product->getReviews()->toArray(), [new Review()]);
        $this->assertNotEquals($product->getColors()->toArray(), [new Color()]);
        $this->assertNotEquals($product->getTags(), ["false"]);
        $this->assertNotEquals($product->getCreatedAt(), new \DateTimeImmutable('2000-01-01'));
    }
    
    public function testAddAndRemoveOrderDetails(): void
    {
        $product = $this->createProduct();
        $orderDetail = new OrderDetail();
        $this->assertEquals($product->getOrderDetails()->toArray(), []);
        $product->addOrderDetail($orderDetail);
        $this->assertEquals($product->getOrderDetails()->toArray(), [$orderDetail]);
        $product->removeOrderDetail($orderDetail);
        $this->assertEquals($product->getOrderDetails()->toArray(), []);
    }
    
    public function testAddAndRemoveReviews(): void
    {
        $product = $this->createProduct();
        $review = new Review();
        $this->assertEquals($product->getReviews()->toArray(), []);
        $product->addReview($review);
        $this->assertEquals($product->getReviews()->toArray(), [$review]);
        $product->removeReview($review);
        $this->assertEquals($product->getReviews()->toArray(), []);
    }
    
    public function testAddAndRemoveColors(): void
    {
        $product = $this->createProduct();
        $color = new Color();
        $this->assertEquals($product->getColors()->toArray(), []);
        $product->addColor($color);
        $this->assertEquals($product->getColors()->toArray(), [$color]);
        $product->removeColor($color);
        $this->assertEquals($product->getColors()->toArray(), []);
    }
    
    public function testAddAndRemovePictures(): void
    {
        $product = $this->createProduct();
        $picture = new Picture();
        $this->assertEquals($product->getPictures()->toArray(), []);
        $product->addPicture($picture);
        $this->assertEquals($product->getPictures()->toArray(), [$picture]);
        $product->removePicture($picture);
        $this->assertEquals($product->getPictures()->toArray(), []);
    }

    /**
     * @param Category $category
     * @param string[] $tags
     * @param OrderDetail[] $orderDetails
     * @param Review[] $reviews
     * @param Picture[] $pictures
     * @param Color[] $colors
     * @param \DateTimeImmutable $createdAt
     *
     * @return Product
     */
    private function createProduct(
        Category $category = null,
        array $tags = array(),
        array $orderDetails = array(),
        array $reviews = array(),
        array $pictures = array(),
        array $colors = array(),
        \DateTimeImmutable $createdAt = null
    ): Product {
        $product = (new Product())
            ->setName("name")
            ->setSlug("slug")
            ->setDescription("description")
            ->setStock(100)
            ->setPrice(15.50)
            ->setCategory($category)
            ->setCreatedAt($createdAt ?? new \DateTimeImmutable("now"))
            ->setTags($tags);
        foreach ($orderDetails as $orderDetail) {
            $product->addOrderDetail($orderDetail);
        }
        foreach ($reviews as $review) {
            $product->addReview($review);
        }
        foreach ($pictures as $picture) {
            $product->addPicture($picture);
        }
        foreach ($colors as $color) {
            $product->addColor($color);
        }
        return $product;
    }
}
