<?php

namespace App\Tests\Entity;

use App\Entity\Color;
use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ColorTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $products = [new Product(), new Product()];
        $color = $this->createColor($products);
        
        $this->assertEquals($color->getName(), "rouge");
        $this->assertEquals($color->getProducts()->toArray(), $products);
        $this->assertEquals($color->getHexadecimals(), ["#f00020"]);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $products = [new Product(), new Product()];
        $color = $this->createColor($products);
        
        $this->assertNotEquals($color->getName(), "noir");
        $this->assertNotEquals($color->getProducts()->toArray(), [new Product()]);
        $this->assertNotEquals($color->getHexadecimals(), ["#000000"]);
    }
    
    public function testAddAndRemoveProducts(): void
    {
        $color = $this->createColor();
        $product = new Product();
        $this->assertEquals($color->getProducts()->toArray(), []);
        $color->addProduct($product);
        $this->assertEquals($color->getProducts()->toArray(), [$product]);
        $color->removeProduct($product);
        $this->assertEquals($color->getProducts()->toArray(), []);
    }
     
    /**
     * @param Product[] $products
     *
     * @return Color
     */
    private function createColor(array $products = array()): Color
    {
        $color = (new Color())
            ->setName("rouge")
            ->setHexadecimals(["#f00020"]);
        foreach ($products as $product) {
            $color->addProduct($product);
        }
        return $color;
    }
}
