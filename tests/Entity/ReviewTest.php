<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use App\Entity\Review;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ReviewTest extends TestCase
{
    public function testAccessorsWithEquals(): void
    {
        $user = new User();
        $product = new Product();
        $review = $this->createReview($user, $product);
        
        $this->assertEquals($review->getComment(), "nul sur 20");
        $this->assertEquals($review->getProduct(), $product);
        $this->assertEquals($review->getUser(), $user);
        $this->assertEquals($review->getGrade(), 5);
    }
    
    public function testAccessorsWithNotEquals(): void
    {
        $user = new User();
        $product = new Product();
        $review = $this->createReview($user, $product);
        
        $this->assertNotEquals($review->getComment(), "cool");
        $this->assertNotEquals($review->getProduct(), (new Product())->setName("false"));
        $this->assertNotEquals($review->getUser(), (new User())->setUsername("false"));
        $this->assertNotEquals($review->getGrade(), 0);
    }

    private function createReview(User $user = null, Product $product = null): Review
    {
        return (new Review())
                ->setUser($user)
                ->setProduct($product)
                ->setComment("nul sur 20")
                ->setGrade(5);
    }
}
