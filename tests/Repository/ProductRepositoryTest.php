<?php

namespace App\Tests\Repository;

use App\Entity\Product;
use App\Model\Filter;
use App\Repository\ProductRepository;
use App\Tests\FixturesTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;

class ProductRepositoryTest extends AbstractRepositoryTest
{
    use FixturesTrait;
    
    protected $repositoryClass = ProductRepository::class;
    
    public function testFindWithFilter()
    {
        ['category1' => $category, //selected category
         'color1' => $color, //selected color
         'product1' => $expectedProduct, //expected product
        ] = $this->loadFixtures(['filter_test']);
        
        
        $filter = new Filter();
        $filter->categories = new ArrayCollection([$category]);
        $filter->color = $color;
        $filter->tag = "filtre-recherche";
        $filter->price = ['min' => 200.5, 'max' => 200.5];
        $filter->grade = ['min' => 10, 'max' => 10];
        
        /** @var Query $query */
        $query = $this->repository->findWithFilter($filter);
        
        /** @var Product[] products */
        $products =  array_map(function ($r) {
            return $r[0];
        }, $query->getResult());
        
        $this->assertEquals(1, count($products));
        $this->assertEquals($expectedProduct, $products[0]);
    }
}
