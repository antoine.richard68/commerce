<?php

namespace App\Tests\Repository;

use App\Repository\AbstractRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @template E
 */
abstract class AbstractRepositoryTest extends WebTestCase
{
    /** @var EntityManagerInterface */
    protected $em;
    
    /**
     * @var string
     */
    protected $repositoryClass = null;
    
    /**
     * @var E
     */
    protected $repository = null;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->em = $this->getContainer()->get(EntityManagerInterface::class);
        $this->repository = $this->getContainer()->get($this->repositoryClass);
    }
}
