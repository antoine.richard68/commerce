<?php

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;

/**
 * @property EntityManagerInterface $em
 */
trait FixturesTrait
{
    public function loadFixtures(array $fixtureFiles): array
    {
        $databaseTool = self::getContainer()->get(DatabaseToolCollection::class)->get();
        $files = array_map(fn ($file) =>  $this->getFixturesPath() . $file . ".yaml", $fixtureFiles);
        return $databaseTool->loadAliceFixture($files);
    }
    
    public function getFixturesPath()
    {
        return __DIR__.'/fixtures/';
    }
    
    public function findInFixtures(string $pattern, array $fixtures): array
    {
        $keys = preg_grep($pattern, array_keys($fixtures));
        $data = [];
        foreach ($keys as $key) {
            array_push($data, $fixtures[$key]);
        }
        return $data;
    }
}
