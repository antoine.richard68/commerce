<?php

namespace App\EventListener;

use App\Kernel;
use App\Annotation\Workflow;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class WorkflowEventListener implements EventSubscriberInterface
{
    
    /** @var AnnotationReader */
    private $reader;
    
    /** @var Registry */
    private $registry;
    
    /** @var EntityManagerInterface */
    private $em;
    
    /** @var UrlGeneratorInterface */
    private $router;
    
    /** @var ContainerInterface */
    private $container;
    
    public function __construct(
        Reader $reader,
        Registry $registry,
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        Kernel $kernel
    ) {
        $this->reader = $reader;
        $this->registry = $registry;
        $this->em = $em;
        $this->router = $router;
        $this->container = $kernel->getContainer();
    }
    
    public static function getSubscribedEvents()
    {
        return [
            ControllerEvent::class => "onKernelController",
         ];
    }
    
    public function onKernelController(ControllerEvent $event)
    {
        /** extract called controller and function */
        if (!is_array($event->getController())) {
            return;
        }
        list($controller, $method) = $event->getController();
        
        /** extract workflow annotation from the function */
        $workflowAnnotation = $this->getMethodAnnotation($controller, $method, Workflow::class);
        if ($workflowAnnotation == null) {
            return;
        }
        
        /** get the repository associate to the entity class */
        $repository = $this->em->getRepository($workflowAnnotation->entityClass);
        
        /** search the entity with request parameters */
        $filter = [];
        foreach ($workflowAnnotation->criteria as $criteria) {
            $filter[$criteria] = $event->getRequest()->get($criteria);
        }
        $entity = $repository->findOneBy($filter);
        if ($entity == null) {
            return;
        }
        
        /** get the workflow associate to the entity */
        $workflow = $this->registry->get($entity);
        if ($workflow == null) {
            return;
        }
        
        /** get the current states */
        $currentStates = array_keys(array_filter($workflow->getMarking($entity)->getPlaces(), fn ($p) => $p));
        
        /** if the state is allready the targeted state */
        if (in_array($workflowAnnotation->place, $currentStates)) {
            return;
        }
        
        /** get all the enabled transitions for the entity */
        $enabledTransitions = $workflow->getEnabledTransitions($entity);
        
        /** find a transition that go to the targeted state */
        foreach ($enabledTransitions as $enabledTransition) {
            if (in_array($workflowAnnotation->place, $enabledTransition->getTos())
                && $workflow->can($entity, $enabledTransition->getName())) {
                $workflow->apply($entity, $enabledTransition->getName());
                $this->em->flush();
                return;
            }
        }
        
        /** if no transition found: search the route associate with the actual state */
        $routes = $this->container->get("router")->getRouteCollection()->all();
        foreach ($routes as $route => $param) {
            $defaults = $param->getDefaults();
            if (isset($defaults['_controller'])) {
                $explode = explode('::', $defaults['_controller']);
                if (count($explode) == 2) {
                    list($controllerService, $controllerMethod) = $explode;
                    $annotation = $this->getMethodAnnotation($controllerService, $controllerMethod, Workflow::class);
                    if (isset($annotation) && in_array($annotation->place, $currentStates)) {
                        $redirectUrl = $this->router->generate($route, $filter);
                        $event->setController(function () use ($redirectUrl) {
                            return new RedirectResponse($redirectUrl);
                        });
                    }
                }
            }
        }
    }
    
    private function getMethodAnnotation($className, string $methodName, string $annotation)
    {
        try {
            $controllerObject = new \ReflectionClass($className);
        } catch (Exception $e) {
            return null;
        }
        $reflectionMethod = $controllerObject->getMethod($methodName);
        return $this->reader->getMethodAnnotation($reflectionMethod, $annotation);
    }
}
