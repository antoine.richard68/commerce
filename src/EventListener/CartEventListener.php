<?php

namespace App\EventListener;

use App\Event\Cart\CartAddEvent;
use App\Event\Cart\CartRemoveEvent;
use App\Service\CartService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CartEventListener implements EventSubscriberInterface
{
    /** @var CartService */
    private $cartService;
    
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }
    
    public static function getSubscribedEvents()
    {
        return [
           CartAddEvent::class => "cartAddHandler",
           CartRemoveEvent::class => "cartRemoveHandler"
        ];
    }
    
    public function cartAddHandler(CartAddEvent $cartAddEvent)
    {
        $this->cartService->addToCart(
            $cartAddEvent->getProduct(),
            $cartAddEvent->getQuantity(),
        );
    }
    
    public function cartRemoveHandler(CartRemoveEvent $cartRemoveEvent)
    {
        $this->cartService->removeToCart(
            $cartRemoveEvent->getProduct(),
            $cartRemoveEvent->getQuantity()
        );
    }
}
