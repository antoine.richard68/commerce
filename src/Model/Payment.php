<?php

namespace App\Model;

use App\Entity\Order;
use App\Entity\User;

class Payment
{
    /** @var User */
    private $user;
    
    /** @var Order */
    private $order;
    
    /** @var string */
    private $cardNumber;
    
    public function getUser(): User
    {
        return $this->user;
    }
    
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }
    
    public function getOrder(): Order
    {
        return $this->order;
    }
    
    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }
    
    public function getCardNumber(): string
    {
        return $this->cardNumber;
    }
    
    public function setCardNumber(string $cardNumber): self
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }
}
