<?php

namespace App\Model;

use App\Entity\Color;
use App\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Filter
{
    /** @var Collection */
    public $categories;
    
    /** @var Color */
    public $color = null;
    
    /**
     * @var array
    */
    public $grade = [
        'min' => 0,
        'max' => 10
    ];
    
    /**
     * @var array
    */
    public $price = [
        'min' => 0,
        'max' => 1000
    ];
    
    /** @var string */
    public $tag = "";
    
    /** @var int */
    public $page = 1;
    
    /** @var int */
    public $limit = 12;
}
