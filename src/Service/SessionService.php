<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

class SessionService
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    public function set(string $key, $value)
    {
        return $this->requestStack->getSession()->set($key, $value);
    }
    
    public function get(string $key, $default = null)
    {
        return $this->requestStack->getSession()->get($key, $default);
    }
}
