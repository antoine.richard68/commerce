<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Workflow\WorkflowInterface;

class OrderService
{
    
    /** @var WorkflowInterface */
    private $orderStateMachine;
    
    /** @var EntityManagerInterface */
    private $em;
    
    public function __construct(WorkflowInterface $orderStateMachine, EntityManagerInterface $em)
    {
        $this->orderStateMachine = $orderStateMachine;
        $this->em = $em;
    }
    
    public function changeState(Order $order, string $transition): bool
    {
        if ($this->orderStateMachine->can($order, $transition)) {
            try {
                $this->orderStateMachine->apply($order, $transition);
            } catch (Exception $e) {
                return false;
            }
            $this->em->flush();
            return true;
        }
        return false;
    }
    
    public function createOrderDetails(
        Order $order,
        array $cart
    ):bool {
        $productRepository = $this->em->getRepository(Product::class);
        foreach ($cart as $product => $quantity) {
            $product = $productRepository->find($product);
            if ($product == null) {
                return false;
            }
            $detail = (new OrderDetail())
                ->setRelatedOrder($order)
                ->setQuantity($quantity)
                ->setProduct($product);
            $this->em->persist($detail);
        }
        $this->em->flush();
        return true;
    }
}
