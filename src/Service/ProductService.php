<?php

namespace App\Service;

use App\Model\Filter;
use App\Repository\ProductRepository;

class ProductService
{
    /** @required */
    public ProductRepository $repository;
    
    /** @required */
    public PaginatorService $paginatorService;
    
    public function __constructor(ProductRepository $repository, PaginatorService $paginatorService)
    {
        $this->repository = $repository;
        $this->paginatorService = $paginatorService;
    }
    
    public function manageFilterRequest(Filter $filter)
    {
        $paginator = $this->paginatorService->paginate(
            $this->repository->findWithFilter($filter),
            $filter->page,
            $filter->limit
        );
        $totalItems = $this->paginatorService->getTotalItems($paginator);
        $totalPages = $this->paginatorService->getTotalPages($paginator);
        
        return [
            'results' => $paginator,
            'pagination' => [
                'currentPage' => $filter->page,
                'currentLimit' => $filter->limit,
                'totalPages' => (int) $totalPages,
                'totalItems' => (int) $totalItems
            ]
        ];
    }
}
