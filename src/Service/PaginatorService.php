<?php

namespace App\Service;

use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginatorService
{
    /**
     * @param Query $query
     * @param int $page
     * @param int $limit
     *
     * @return Paginator
     */
    public function paginate(Query $query, int $page, int $limit): Paginator
    {
        $paginator = new Paginator($query);
        $paginator->getQuery()
                ->setFirstResult($limit * ($page-1))
                ->setMaxResults($limit);
        return $paginator;
    }
    
    public function getTotalItems(Paginator $paginator): int
    {
        return $paginator->count();
    }
    
    public function getTotalPages(Paginator $paginator): int
    {
        return ceil($this->getTotalItems($paginator) / $paginator->getQuery()->getMaxResults());
    }
}
