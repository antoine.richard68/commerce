<?php

namespace App\Service;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CartService
{
    const SESSION_KEY = "cart";
    
    private $session;
    
    private $productRepository;
    
    private $normalizer;

    public function __construct(
        SessionService $session,
        ProductRepository $productRepository,
        NormalizerInterface $normalizer
    ) {
        $this->session = $session;
        $this->productRepository = $productRepository;
        $this->normalizer = $normalizer;
    }
    
    public function addToCart(Product $product, int $quantity = 1)
    {
        /** @var array */
        $cart = $this->getCart();
        if (array_key_exists($product->getId(), $cart)) {
            $cart[$product->getId()] += $quantity;
        } else {
            $cart[$product->getId()] = $quantity;
        }
        $this->setCart($cart);
    }
    
    public function removeToCart(Product $product, int $quantity = 1)
    {
        /** @var array */
        $cart = $this->getCart();
        if (array_key_exists($product->getId(), $cart)) {
            $cart[$product->getId()] -= $quantity;
            if ($cart[$product->getId()] <= 0) {
                unset($cart[$product->getId()]);
            }
        } else {
            return;
        }
        $this->setCart($cart);
    }
    
    public function emptyCart()
    {
        $this->setCart([]);
    }
    
    public function getCart()
    {
        return $this->session->get(self::SESSION_KEY, []);
    }
    
    public function setCart(array $cart)
    {
        return $this->session->set(self::SESSION_KEY, $cart);
    }
    
    public function getCartLine()
    {
        $cart = $this->getCart();
        $products = $this->normalizer->normalize(
            $this->productRepository->findBy(["id" => array_keys($cart)]),
            null,
            ['groups' => 'list_products']
        );
        return [
            "products" => $products ,
            "quantities" => $cart,
            "total" => round(
                array_sum(array_map(fn ($product) => $product["price"] * $cart[$product["id"]], $products)),
                2
            )
        ];
    }
}
