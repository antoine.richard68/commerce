<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Event\Cart\CartAddEvent;
use App\Event\Cart\CartRemoveEvent;
use App\Repository\ProductRepository;
use App\Service\CartService;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("api/cart/add", name="cart_add", methods={"POST"})
     */
    public function add(
        Request $request,
        ProductRepository $productRepository,
        EventDispatcherInterface $eventDispatcher
    ): JsonResponse {
        /** @var Product */
        $product = $productRepository->find($request->get("id"));
        $eventDispatcher->dispatch(new CartAddEvent($product, $request->get("quantity")));
        $response = $this->json([
            "product" =>$product,
            "quantity" => $request->get("quantity")], 200, [], ["groups" => "cart"]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
    
    /**
     * @Route("api/cart/remove", name="cart_remove", methods={"POST"})
     */
    public function remove(
        Request $request,
        ProductRepository $productRepository,
        EventDispatcherInterface $eventDispatcher
    ): JsonResponse {
        /** @var Product */
        $product = $productRepository->find($request->get("id"));
        $eventDispatcher->dispatch(new CartRemoveEvent($product, $request->get("quantity")));
        $response = $this->json([
            "product" =>$product,
            "quantity" => $request->get("quantity")], 200, [], ["groups" => "cart"]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
    
    /**
     * @Route("api/cart", name="cart", methods={"GET"})
     */
    public function getCart(CartService $cartService): JsonResponse
    {
        $response = $this->json($cartService->getCartLine(), 200);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
