<?php

namespace App\Controller\Api;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CategoryController extends AbstractController
{
    /**
     * @Route("/api/categories", name="api_categories", methods={"GET"})
     */
    public function listCategories(CategoryRepository $categoryRepository): JsonResponse
    {
        $response = $this->json($categoryRepository->findAll(), 200, [], ["groups" => "list_categories"]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
