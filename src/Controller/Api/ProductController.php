<?php

namespace App\Controller\Api;

use App\Form\FilterFormType;
use App\Model\Filter;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/api/products", name="products_filter", methods={"GET"})
     */
    public function filter(Request $request, ProductService $service): JsonResponse
    {
        $filter = new Filter();
        $form = $this->createForm(FilterFormType::class, $filter);
        $form->submit($request->query->all());
        $filter = $form->getData();
        $response = $this->json($service->manageFilterRequest($filter), 200, [], ["groups" => "list_products"]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
