<?php

namespace App\Controller\Api;

use App\Repository\CategoryRepository;
use App\Repository\ColorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ColorController extends AbstractController
{
    /**
     * @Route("/api/colors", name="api_colors", methods={"GET"})
     */
    public function listColors(ColorRepository $colorRepository): JsonResponse
    {
        $response = $this->json($colorRepository->findAll(), 200, [], ["groups" => "list_colors"]);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
