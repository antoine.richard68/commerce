<?php

namespace App\Controller;

use App\Entity\Order;
use App\Annotation\Workflow;
use App\Model\Payment;
use App\Service\CartService;
use App\Form\OrderPaymentFormType;
use App\Form\OrderInformationsFormType;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class OrderController extends AbstractController
{
    
    /**
     * @Route("/order/cart", name="order_cart", methods={"GET"})
     */
    public function orderCart(CartService $cartService): Response
    {
        return $this->render('order/cart.html.twig', [
            'cart' => $cartService->getCartLine()
        ]);
    }
    
    /**
     * @Route("/order/creation", name="order_creation")
     */
    public function createOrderEntity(
        EntityManagerInterface $em,
        OrderService $orderService,
        CartService $cartService
    ): Response {
        $order = new Order();
        $order->setCustomer($this->getUser());
        $order->setAddress($this->getUser()->getAddress());
        $em->persist($order);
        $em->flush();
        if ($orderService->createOrderDetails($order, $cartService->getCart())) {
            return $this->redirectToRoute('order_informations', ["id" => $order->getId()]);
        }
        return $this->redirectToRoute('order_cart');
    }
    
    /**
     * @Route("/order/informations/{id}", name="order_informations")
     * @Workflow(place="waiting_informations", entityClass=Order::class)
     */
    public function orderInformations(Order $order, Request $request, OrderService $orderService): Response
    {
        $form = $this->createForm(OrderInformationsFormType::class, $order);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            if ($orderService->changeState($order, "validate_informations")) {
                return $this->redirectToRoute('order_payment', ["id" => $order->getId()]);
            }
        }
        return $this->render('order/informations.html.twig', [
            'order' => $order,
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/order/payment/{id}", name="order_payment")
     * @Workflow(place="waiting_payment", entityClass=Order::class)
     */
    public function orderPayment(
        Order $order,
        Request $request,
        OrderService $orderService,
        CartService $cartService
    ): Response {
        $payment = new Payment();
        $payment->setUser($this->getUser());
        $payment->setOrder($order);
        $form = $this->createForm(OrderPaymentFormType::class, $payment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $payment = $form->getData();
            if ($orderService->changeState($order, "validate_payment")) {
                $cartService->emptyCart();
                return $this->redirectToRoute('order_email', ["id" => $order->getId()]);
            }
        }
        return $this->render('order/payment.html.twig', [
            'order' => $order,
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/order/email/{id}", name="order_email")
     * @Workflow(place="waiting_email", entityClass=Order::class)
     */
    public function sendEmail(Order $order, OrderService $orderService, MailerInterface $mailer): Response
    {
        $email = (new Email())
            ->from("exemple@exemple.fr")
            ->to($order->getCustomer()->getEmail())
            ->replyTo("exemple@exemple.fr")
            ->subject("Confirmation de la commande")
            ->html("<p>Bien ouej</p>");
        $mailer->send($email);
        if ($orderService->changeState($order, "validate_email")) {
            return $this->redirectToRoute('order_confirmation', ["id" => $order->getId()]);
        }
        return $this->redirectToRoute('order_cart');
    }
    
    /**
     * @Route("/order/confirmation/{id}", name="order_confirmation")
     * @Workflow(place="confirmed", entityClass=Order::class)
     */
    public function orderConfirmation(Order $order, MailerInterface $mailer): Response
    {
        return $this->render('order/confirmation.html.twig', [
            'order' => $order,
        ]);
    }
}
