<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use App\Model\Filter;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends AbstractRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }
    
    /**
     * @param Filter $filter
     *
     * @return Query
     */
    public function findWithFilter(Filter $filter): Query
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->andWhere('p.price BETWEEN :priceMin AND :priceMax')
            ->setParameter('priceMin', $filter->price["min"])
            ->setParameter('priceMax', $filter->price['max'])
            ->leftJoin('p.reviews', 'r')
            ->addSelect('AVG(COALESCE(r.grade, 0.00)) as avg_grade')
            ->having('avg_grade BETWEEN :gradeMin AND :gradeMax')
            ->setParameter('gradeMin', $filter->grade['min'])
            ->setParameter('gradeMax', $filter->grade['max'])
            ->groupBy('p.id')
            ->orderBy('p.createdAt', 'DESC');
                
        if (!$filter->categories->isEmpty()) {
            $queryBuilder->leftJoin('p.category', 'ca')
                ->andWhere('ca.id IN (:categories)')
                ->setParameter('categories', $filter->categories->map(fn (Category $c) => $c->getId())->toArray());
        }
        
        if ($filter->color != null) {
            $queryBuilder->leftJoin('p.colors', 'co')
                ->andWhere('co.id IN (:color)')
                ->setParameter('color', $filter->color->getId());
        }
        
        if ($filter->tag != "") {
            $queryBuilder->andWhere('p.tags LIKE :tag')
                ->setParameter('tag', '%'.$filter->tag.'%');
        }
        //dd($queryBuilder);
        return $queryBuilder->getQuery();
    }
    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
