<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\User;
use App\Entity\Color;
use App\Entity\Order;
use App\Entity\Review;
use App\Entity\Picture;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\OrderDetail;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    
    private $passwordHasher;
    private $orderStateMachine;
    private $faker;

    public function __construct(UserPasswordHasherInterface $passwordHasher, WorkflowInterface $orderStateMachine)
    {
        $this->passwordHasher = $passwordHasher;
        $this->orderStateMachine = $orderStateMachine;
        $this->faker = Faker\Factory::create('fr_FR');
    }
    
    
    public function load(ObjectManager $manager)
    {
              
        /**
        * Colors
        */
        $colors = [];
        for ($i=0; $i<5; $i++) {
            $colors[$i] = new Color();
            $hexas= [];
            $rand = random_int(1, 3);
            for ($j=0; $j<$rand; $j++) {
                $hexas[] = $this->faker->hexColor();
            }
            $colors[$i]
                ->setName($this->faker->unique()->safeColorName())
                ->setHexadecimals($hexas);
            $manager->persist($colors[$i]);
        }
        
        /**
        * Users
        */
        $users = [];
        $admin = new User();
        $admin
            ->setUsername("antoine")
            ->setEmail("antoine@antoine.fr")
            ->setAddress($this->faker->streetAddress())
            ->setRoles(["ROLE_ADMIN"])
            ->setPassword($this->passwordHasher->hashPassword(
                $admin,
                "12345678"
            ))
            ->setIsVerified(true);
        $manager->persist($admin);
        for ($i=0; $i<5; $i++) {
            $users[$i] = new User();
            $users[$i]
                ->setUsername($this->faker->unique()->userName())
                ->setAddress($this->faker->streetAddress())
                ->setEmail($this->faker->unique()->email())
                ->setPassword($this->passwordHasher->hashPassword(
                    $admin,
                    $this->faker->password()
                ))
                ->setIsVerified(true);
            $manager->persist($users[$i]);
        }
        
        /**
        * Categories
        */
        $categories = [];
        for ($i=0; $i<5; $i++) {
            $picture = new Picture();
            $picture
                ->setTitle("Banane")
                ->setDescription("une bannana")
                ->setPath("banane.jpg");
            $manager->persist($picture);
            
            $faIconsSample = array("far fa-file", "fab fa-bitcoin", "fas fa-atom", "fas fa-archive");
            $categories[$i] = new Category();
            $categories[$i]
                ->setName($this->faker->unique()->word())
                ->setSlug($this->faker->unique()->slug())
                ->setDescription($this->faker->paragraph(3))
                ->setFaIcon($faIconsSample[array_rand($faIconsSample)])
                ->addPicture($picture);
            $manager->persist($categories[$i]);
        }
        
        /**
        * Products
        */
        $products = [];
        for ($i=0; $i<20; $i++) {
            $picture = new Picture();
            $picture
                ->setTitle("Banane")
                ->setDescription("une bannana")
                ->setPath("banane.jpg");
            $manager->persist($picture);
            
            
            $products[$i] = new Product();
            $products[$i]
                ->setName($this->faker->unique()->word())
                ->setSlug($this->faker->unique()->slug())
                ->setDescription($this->faker->paragraph(3))
                ->setStock($this->faker->numberBetween(0, 200))
                ->setPrice($this->faker->randomFloat(2, 1, 500))
                ->setTags($this->faker->words(3, false))
                ->setCategory($categories[array_rand($categories)])
                ->addColor($colors[array_rand($colors)])
                ->setCreatedAt(new \DateTimeImmutable('now'))
                ->addPicture($picture);
            $manager->persist($products[$i]);
        }
        
        /**
        * Reviews
        */
        $reviews = [];
        for ($i=0; $i<100; $i++) {
            $reviews[$i] = new Review();
            $reviews[$i]
                ->setUser($users[array_rand($users)])
                ->setProduct($products[array_rand($products)])
                ->setGrade($this->faker->numberBetween(0, 10))
                ->setComment($this->faker->paragraph(3));
            $manager->persist($reviews[$i]);
        }
        
        /**
        * Orders
        */
        $orders = [];
        for ($i=0; $i<15; $i++) {
            $orders[$i] = new Order();
            $orders[$i]
                ->setCustomer($users[array_rand($users)])
                ->setAddress($this->faker->streetAddress())
                ->setState(array_rand($this->orderStateMachine->getDefinition()->getPlaces()));
            $manager->persist($orders[$i]);
            /**
             * OrderDetails
             */
            $details = [];
            for ($j=0; $j<5; $j++) {
                $details[$j] = new OrderDetail();
                $details[$j]
                    ->setRelatedOrder($orders[$i])
                    ->setProduct($products[array_rand($products)])
                    ->setQuantity($this->faker->numberBetween(1, 5))
                    ->setDiscount($this->faker->numberBetween(0, 100));
                $manager->persist($details[$j]);
            }
        }
        $manager->flush();
    }
}
