<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Color;
use App\Model\Filter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categories', EntityType::class, [
                'class' => Category::class,
                'multiple' => true,
            ])
            ->add('color', EntityType::class, [
                'class' => Color::class,
            ])
            ->add('grade', CollectionType::class, [
                'allow_add' => false,
                'entry_type' => NumberType::class
            ])
            ->add('price', CollectionType::class, [
                'allow_add' => false,
                'entry_type' => NumberType::class
            ])
            ->add('tag', TextType::class)
            ->add('page', NumberType::class)
            ->add('limit', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
        ]);
    }
}
