<?php

namespace App\Twig;

use Twig\TwigFilter;
use App\Entity\Order;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Symfony\Component\Workflow\WorkflowInterface;

class OrderWorkflowExtension extends AbstractExtension
{
    /** @var WorkflowInterface */
    private $workflow;
    
    /** @var array */
    private $showedPlacesMetadata;
    
    public function __construct(WorkflowInterface $orderStateMachine)
    {
        $this->workflow = $orderStateMachine;
        $places = $this->workflow->getDefinition()->getPlaces();
        $this->showedPlacesMetadata = [];
        foreach ($places as $place) {
            $data = $this->workflow->getMetadataStore()->getPlaceMetadata($place);
            if (isset($data["twig"]) && $data["twig"]["show"]) {
                $this->showedPlacesMetadata[$place] = $data;
            }
        }
    }
        
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            //new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_showed_order_workflow_places_metadata', [$this, 'getShowedPlacesMetadata']),
            new TwigFunction('place_is_active', [$this, 'isActive']),
            new TwigFunction('place_has_gap', [$this, 'hasGap']),
            new TwigFunction('can_link', [$this, 'canLink']),
        ];
    }

    public function getShowedPlacesMetadata(): array
    {
        return $this->showedPlacesMetadata;
    }
    
    
    public function isActive(?Order $order, $state): bool
    {
        if (!$order) {
            return false;
        }
        return $order->getState() == $state;
    }
    
    public function hasGap(?Order $order, $state): bool
    {
        if (!$order) {
            return true;
        }
        return (
            $this->getPlaceIndex($order->getState()) <
            $this->getPlaceIndex($state)
        );
    }
    
    public function canLink(?Order $order, $state): bool
    {
        if (!$order) {
            return false;
        }
        foreach ($this->workflow->getEnabledTransitions($order) as $transition) {
            foreach ($transition->getTos() as $to) {
                if ($to == $state) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private function getPlaceIndex(string $state): int
    {
        return array_search($state, array_keys($this->showedPlacesMetadata));
    }
}
