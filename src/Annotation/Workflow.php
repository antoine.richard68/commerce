<?php

namespace App\Annotation;

/**
 * Annotation class for @Workflow().
 *
 * @Annotation
 */
class Workflow
{
    /**
     * @var string
     */
    public $place;
    
    /**
     * @var string
     */
    public $entityClass;
    
    /**
     * @var array
     */
    public $criteria = ["id"];
}
