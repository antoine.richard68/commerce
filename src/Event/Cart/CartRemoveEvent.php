<?php

namespace App\Event\Cart;

use App\Entity\Product;

class CartRemoveEvent
{
    private Product $product;
    
    private int $quantity;

    public function __construct(Product $product, int $quantity = 1)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
    
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
