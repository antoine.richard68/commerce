import axios from "axios";
import { serialize } from "../fonctions/utils";

export async function searchWithFilter(filter) {
    return axios.get("http://localhost:8000/api/products?" + serialize(filter));
}

export async function addToCart(id, quantity) {
    return axios.post("http://localhost:8000/api/cart/add", {}, { params: {
       "id": id,
       "quantity": quantity 
    }});
}

export async function removeToCart(id, quantity) {
    return axios.post("http://localhost:8000/api/cart/remove", {}, { params: {
       "id": id,
       "quantity": quantity 
    }});
}

export async function getCart() {
    return axios.get("http://localhost:8000/api/cart");
}
