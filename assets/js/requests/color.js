import axios from "axios";

export async function listColors() {
    return axios.get("http://localhost:8000/api/colors");
}