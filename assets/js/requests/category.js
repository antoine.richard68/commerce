import axios from "axios";

export async function listCategories() {
    return axios.get("http://localhost:8000/api/categories");
}