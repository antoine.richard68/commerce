import '@fortawesome/fontawesome-free';


import register from 'preact-custom-element';
import Cart from './components/Cart.jsx';
import Catalogue from './components/Catalogue/Catalogue.jsx';
import Notification from './components/Notification.jsx';
import Search from './components/Search.jsx';

register(Search, 'search-bar');
register(Catalogue, 'catalogue-viewer');
register(Notification, 'notification-container');
register(Cart, 'shopping-cart');