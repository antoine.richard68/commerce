import { h, Component, Fragment, createRef } from "preact";
import { addToCart, getCart, removeToCart } from "../requests/product";
import { cartChangeService } from "../services/cartChange.service";
import { notificationService } from "../services/notification.service";

export default class Cart extends Component {
    
    constructor() {
        super();
        this.state = {
            isOpen: false,
            products : [],
            total : 0.0
        }
        this.subscription;
    }
    
    componentDidMount() {
        this.loadCart();
        document.addEventListener("click", () => this.closeCart());
        this.subscription = cartChangeService.onCartChange()
            .subscribe(cart => {
                this.loadCart();
            });
    }
    
    componentWillUnmount() {
        this.subscription.unsubscribe();
    }
    
    loadCart() {
        getCart().then(response => {
            if(response.status === 200) {
                let products = response.data.products.map(product => {return {...product, quantity: response.data.quantities[product.id]}})
                this.setState({
                    products: products,
                    total: parseFloat(response.data.total)
                })
            }
            
        })
    }
    
        
    onTrigger(event) {
        event.stopPropagation();
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
        
    closeCart() {
        this.setState({
            isOpen: false
        });
    }
    
    add(idProduct, quantity = 1) {
        addToCart(idProduct, quantity).then(response =>
            {
                if (response.status === 200) {
                    let name = response.data.product.name;
                    let quantity = response.data.quantity;
                    notificationService.success(`${quantity}X ${name} ajouté au panier!`, {autoClose: true});
                    cartChangeService.add();
                } else {
                    notificationService.error('Erreur lors de l\'ajout du produit au panier!', {autoClose: true});
                }
            }
        );
    }
    
    remove(idProduct, quantity = 1) {
        removeToCart(idProduct, quantity).then(response =>
            {
                if (response.status === 200) {
                    let name = response.data.product.name;
                    let quantity = response.data.quantity;
                    notificationService.success(`${quantity}X ${name} retiré du panier!`, {autoClose: true});
                    cartChangeService.remove();
                } else {
                    notificationService.error('Erreur lors de la suppression du produit au panier!', {autoClose: true});
                }
            }
        );
    }

    render() {
        return (
            <div class={`cart dropdown is-right ${this.state.isOpen ? "is-active" : "" }`}>
                <div class="cart-trigger dropdown-trigger" data-products={(this.state.products.length < 100) ? this.state.products.length : "99+"}>
                    <button class="cart-button button" aria-haspopup="true" aria-controls="dropdown-menu2" onClick={(event) => this.onTrigger(event)}>
                        <span>
                            <i class="fas fa-shopping-cart"></i>
                        </span>
                    </button>
                </div>
                <div class="dropdown-menu" id="dropdown-menu2" role="menu" onClick={(event) => event.stopPropagation()}>
                    <div class="dropdown-content">
                        <div class="dropdown-item">
                            <h3 class="title is-3">Panier</h3>
                            {!this.state.products.length ? <p>Votre panier est vide.</p> : null}
                            <ul>
                                {
                                    this.state.products.map(product => {
                                        return(
                                            <li class="my-2">
                                                <img style={{display: "inline-block", verticalAlign: "middle"}} class="image is-32x32" src={"http://localhost:8000/uploads/" + product.pictures[0].path} alt="Placeholder image" />
                                                {product.name}
                                                <button onClick={() => this.add(product.id)}>+</button>
                                                {product.quantity}
                                                <button onClick={() => this.remove(product.id)}>-</button>
                                                <button onClick={() => this.remove(product.id, product.quantity)}>X</button>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <a href="/">Rechercher des produits</a>
                        </div>
                        <hr class="dropdown-divider" />
                        <div class="dropdown-item">
                            <b>Total: {this.state.total}€</b>
                        </div>
                        <hr class="dropdown-divider" />
                        <div class="dropdown-item">
                            <div class="buttons">
                                <button onClick={() => location.href = "/order/cart"} class="button is-primary" disabled={!this.state.products.length}>Passer la commande</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}