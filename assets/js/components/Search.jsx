
import { h, Component, Fragment, createRef } from "preact";

export default class Search extends Component {
    constructor() {
        super();
        this.state = {
            input: "",
            data: []
        }
        this.inputRef = createRef();
    }

    handleChange(event) {
        this.setState({
            input: event.target.value,
            data: [event.target.value, event.target.value]
        });

    }

    handleErase() {
        this.setState({
            input: "",
            data: []
        }, () => { this.inputRef.current.focus() });
    }

    render() {
        return (
            <div class={"dropdown " +  (this.state.data.length === 0 ? "" : "is-active")}>
                <div class="dropdown-trigger">
                    <div class="field">
                        <p class="control is-expanded has-icons-left">
                            <input ref={this.inputRef} onInput={this.handleChange.bind(this)} value={this.state.input} class="input" type="text" placeholder="Rechercher un article" />
                            <span class="icon is-left">
                                <i class="fas fa-search" aria-hidden="true"></i>
                            </span>
                        </p>
                    </div>
                </div>
                <div class="dropdown-menu search-dropdown-menu" id="dropdown-menu" role="menu">
                    <div class="dropdown-content search-dropdown-content">
                        {this.state.data.map(data => {
                            return (
                                <a class="dropdown-item">
                                    <span class="mx-2">
                                        <i class="fas fa-book" aria-hidden="true"></i>
                                    </span>
                                    {data}
                                </a>
                            );
                        })}
                        <div class="dropdown-item">
                            <button onClick={this.handleErase.bind(this)} class="button is-link is-outlined is-fullwidth">
                                Effacer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}