import { h, Component, Fragment, createRef } from "preact";
import { notificationService, notificationType } from "../services/notification.service";

export default class Notification extends Component {
    
    constructor() {
        super();
        this.state = {
            notifications: []
        };
        this.subscription;
    }
    
    componentDidMount() {
        this.subscription = notificationService.onNotification(this.props.id)
            .subscribe(notification => {
                this.setState({ notifications: [...this.state.notifications, notification] });
                if (notification.autoClose) {
                    setTimeout(() => this.removeNotification(notification), 3000);
                }
            });
    }
    
    componentWillUnmount() {
        this.subscription.unsubscribe();
    }
    
    removeNotification(notification) {
        this.setState({ notifications: this.state.notifications.filter(x => x !== notification) });
    }
    
    cssClasses(notification) {
        if (!notification) return;

        let classes = [];   
        const notificationTypeClass = {
            [notificationType.success]: 'notification is-success',
            [notificationType.error]: 'notification is-danger',
            [notificationType.info]: 'notification is-info',
            [notificationType.warning]: 'notification is-warning'
        }

        classes.push(notificationTypeClass[notification.type]);

        return classes.join(' ');
    }

    render() {
        if(!this.state.notifications.length) return(null);
        return (
            <Fragment>
                {this.state.notifications.map((notification, index) => {
                    return(
                        <div key={index} class={this.cssClasses(notification)}>
                            <button class="delete" onClick={() => this.removeNotification(notification)}></button>
                            {notification.message}
                        </div>
                    );
                })}
            </Fragment>            
        );
    }
}