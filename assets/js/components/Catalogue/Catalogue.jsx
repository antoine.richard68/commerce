import { h, n, Component, Fragment, createRef } from "preact";
import { listCategories } from "../../requests/category";
import { listColors } from "../../requests/color";
import { addToCart, getCart, searchWithFilter } from "../../requests/product";
import { cartChangeService } from "../../services/cartChange.service";
import { notificationService } from "../../services/notification.service";
import Loader from "../Loader";
import CategoryFilter from "./Filter/CategoryFilter";
import ColorFilter from "./Filter/ColorFilter";
import SliderFilter from "./Filter/SliderFilter";
import TagFilter from "./Filter/TagFilter";
import Paginator from "./Viewer/Paginator";
import ProductCard from "./Viewer/ProductCard";

export default class Catalogue extends Component {
    constructor() {
        super();
        this.state = {
            categories: [],
            colors: [],
            results: [],
            isLoading: true,
            pagination : {
                page: 1,
                limit: 12,
                totalPages: 0,
                totalItems: 0,
            },
            filter : {
                categories: [],
                color: "",
                tag: "",
                price: { min: 0, max: 1000 },
                grade: { min: 0, max: 10 },
            },
        }
        this.previousFilter = null;
        this.ref = createRef();
    }

    componentDidMount() {
        listCategories().then(response => {
            if (response.status === 200) {
                this.setState({
                    categories: response.data
                })
            }
        });
        listColors().then(response => {
            if (response.status === 200) {
                this.setState({
                    colors: response.data
                })
            }
        });
        this.onSubmit();
    }

    onSelectedCategoriesChange(categories) {
        this.setState({
            filter: {...this.state.filter, categories: categories}
        });
    }

    onColorChange(color) {
        this.setState({
            filter: {...this.state.filter, color: color}
        });
    }

    onTagChange(tag) {
        this.setState({
            filter: {...this.state.filter, tag: tag}
        });
    }

    onPriceChange(price) {
        this.setState({
            filter: {...this.state.filter, price: price}
        });
    }

    onGradeChange(grade) {
        this.setState({
            filter: {...this.state.filter, grade: grade}
        });
    }
    
    onPageChange(page) {
        this.setState({
            pagination: {...this.state.pagination, page: page}
        }, this.onSubmit(true));
    }

    prepareSubmit(pageChange = false, callback = null) {
        this.previousFilter = (pageChange && this.previousFilter !== null) ? this.previousFilter : this.state.filter;
        this.setState({
            isLoading: true,
            pagination: {...this.state.pagination, page: pageChange ? this.state.pagination.page : 1}
        }, callback);
    }
    
    cart(idProduct, quantity = 1) {
        addToCart(idProduct, quantity).then(response =>
            {
                if (response.status === 200) {
                    let name = response.data.product.name;
                    let quantity = response.data.quantity;
                    notificationService.success(`${quantity}X ${name} ajouté au panier!`, {autoClose: true});
                    cartChangeService.add();
                } else {
                    notificationService.error('Erreur lors de l\'ajout du produit au panier!', {autoClose: true});
                }
            }
        );
    }

    onSubmit(pageChange = false) {
        this.prepareSubmit(pageChange,
            () => searchWithFilter({
                ...this.previousFilter,
                page: this.state.pagination.page,
                limit: this.state.pagination.limit
            }).then(response => {
                this.setState({
                    results: response.data.results,
                    pagination: {...this.state.pagination, 
                        totalPages : response.data.pagination.totalPages,
                        totalItems : response.data.pagination.totalItems
                    },
                    isLoading: false,
                }, () => this.ref.current.scrollIntoView({behavior: "smooth"}))
            })
        );
    }
    
    render() {
        let totalItems;
        if (!this.state.isLoading) {
            totalItems = <h2 class="subtitle">Résultat: {this.state.pagination.totalItems} produits trouvés - Page {this.state.pagination.page}</h2>
        }
        let loader = this.state.isLoading ? <Loader text="Chargement des produits"></Loader> : null;
        let paginator;
        if(this.state.pagination.totalItems > 0) {
            paginator = <Paginator
                            current={this.state.pagination.page}
                            total={this.state.pagination.totalPages}
                            onChange={this.onPageChange.bind(this)} />
            
        }
        return (
            <div className={"catalogue"} ref={this.ref}>
                {loader}
                <div className={"catalogue-filter has-background-light"}>
                    <h2 className={"title is-2"}>Filtre</h2>
                    <div className={"filter"}>
                        <span className="mb-1">Catégories</span>
                        <CategoryFilter
                            categories={this.state.categories}
                            onChange={this.onSelectedCategoriesChange.bind(this)} >
                        </CategoryFilter>
                    </div>
                    <div className={"filter"}>
                        <span className="mb-1">Couleur</span>
                        <ColorFilter
                            colors={this.state.colors}
                            onChange={this.onColorChange.bind(this)} >
                        </ColorFilter>
                    </div>
                    <div className={"filter"}>
                        <span className="mb-1">Prix</span>
                        <SliderFilter
                            min="0"
                            max="1000"
                            minValue={this.state.filter.price.min}
                            maxValue={this.state.filter.price.max}
                            onChange={this.onPriceChange.bind(this)}>
                            <i class="fas fa-euro-sign ml-1"></i>
                        </SliderFilter>
                    </div>
                    <div className={"filter"}>
                        <span className="mb-1">Note</span>
                        <SliderFilter
                            min="0"
                            max="10"
                            minValue={this.state.filter.grade.min}
                            maxValue={this.state.filter.grade.max}
                            onChange={this.onGradeChange.bind(this)}>
                            <i class="fas fa-star ml-1"></i>
                        </SliderFilter>
                    </div>
                    <div className={"filter"}>
                        <span className="mb-1">Tag</span>
                        <TagFilter onChange={this.onTagChange.bind(this)}></TagFilter>
                    </div>
                    <div class="filter buttons">
                        <button class="button is-primary" onClick={this.onSubmit.bind(this, false)}>Chercher</button>
                        <button class="button is-link">Effacer</button>
                    </div>
                </div>
                <div className={"catalogue-viewer"}>

                    <h2 className={"title is-2"}>Produits</h2>
                    {totalItems}
                    <div class="cards">
                        {
                            this.state.results.map(result => {
                                return (
                                    <ProductCard
                                        addToCart={this.cart.bind(this)}
                                        product={result[0]}
                                        options={{
                                            avgGrade: result.avg_grade
                                        }} >
                                    </ProductCard>
                                )
                            })
                        }
                    </div>
                    {paginator}
                </div>
            </div>
        );
    }
}