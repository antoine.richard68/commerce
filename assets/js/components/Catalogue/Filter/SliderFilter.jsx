import { h, Component, Fragment } from "preact";

export default class SliderFilter extends Component {
        
    onChange() {
        this.props.onChange(this.state);
    }
    
    onMinChange(event) {
        this.props.onChange({
            min: Math.min(Number(event.target.value), this.props.maxValue - 1), 
            max: this.props.maxValue
            }
        );
    }
    onMaxChange(event) {
        this.props.onChange({
            min: this.props.minValue, 
            max: Math.max(Number(event.target.value), this.props.minValue + 1)
            }
        );
    }
    
    
    render() {
        return(
            <div className={"slider-container"}>
                <input
                    type="range"
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.minValue}
                    onInput={this.onMinChange.bind(this)}
                    style={{ zIndex: this.props.minValue > this.props.max - 100 && "5" }}
                    className="thumb thumb--left"
                />
                <input
                    type="range"
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.maxValue}
                    onInput={this.onMaxChange.bind(this)}
                    className="thumb thumb--right"
                />
                <div className="slider">
                    <div className="slider__track" />
                    <div className="slider__range" />
                    <div className="slider__left-value has-text-black">{this.props.minValue}{this.props.children}</div>
                    <div className="slider__right-value has-text-black">{this.props.maxValue}{this.props.children}</div>
                </div>
            </div>
        );
    }
    
}