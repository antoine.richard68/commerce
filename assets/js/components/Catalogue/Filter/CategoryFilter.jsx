import { h, Component, Fragment } from "preact";

export default class CategoryFilter extends Component {
    
    onChange(event) {
        this.props.onChange(Array.from(event.target.selectedOptions).map(option => {return option.value}));
    }
    
    render() {
        return(
            <div class="select is-multiple">
                <select multiple size="6" onChange={this.onChange.bind(this)}>
                    {
                        this.props.categories.map(category => {
                            return (
                                <option value={category.id}><i className={category.fa_icon + " mr-1"}></i>{category.name}</option>
                            );
                        })
                    }
                </select>
            </div>
        );
    }
}