import { h, Component, Fragment } from "preact";

export default class ColorFilter extends Component {
    
    onChange(event) {
        this.props.onChange(event.target.selectedOptions[0].value);
    }
    
    render() {
        return(
            <div class="select" >
                <select onChange={this.onChange.bind(this)}>
                    <option>Toutes</option>
                    {
                        this.props.colors.map(color => {
                            return (
                                <option value={color.id} style={{
                                    backgroundColor: color.hexadecimals[0]
                                }}>{color.name}</option>
                            );
                        })
                    }
                </select>
            </div>
        );
    }
    
}