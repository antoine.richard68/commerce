import { h, Component, Fragment } from "preact";

export default class TagFilter extends Component {
    
    onInput(event) {
        this.props.onChange(event.target.value);
    }
    
    render() {
        return(
            <input onInput={this.onInput.bind(this)} class="input" type="text" placeholder="vintage, mode, chic, ..." />
        );
    }
    
}