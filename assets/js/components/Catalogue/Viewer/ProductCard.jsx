import { h, Component, Fragment } from "preact";
import { formatDate } from "../../../fonctions/utils";

export default class ProductCard extends Component {

    
    render() {
        let avgGrade = parseFloat(this.props.options.avgGrade).toFixed(1);
        let gradeColor = avgGrade > 7.0 ? "green" : (avgGrade > 4.0 ? "blue" : "orange");
        let multipleImages;
        if (this.props.product.pictures.length > 1) {
            multipleImages = <i class="far fa-images multiple-images"></i>
        }
        return (
            <a class="card">
                <div class="card-image">
                    {multipleImages}
                    <figure class="image is-4by3">
                        <img src={"http://localhost:8000/uploads/" + this.props.product.pictures[0].path} alt="Placeholder image" />
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <i className={this.props.product.category.fa_icon}></i>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">{this.props.product.name}</p>
                            <p class="subtitle is-6">{this.props.product.category.name}</p>
                        </div>
                        <div class="media-right">
                            <div class="single-chart">
                                <svg viewBox="0 0 36 36" class={"circular-chart " + gradeColor}>
                                    <path class="circle-bg"
                                        d="M18 2.0845
                                a 15.9155 15.9155 0 0 1 0 31.831
                                a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                    <path class="circle"
                                        stroke-dasharray={(avgGrade * 10.) + ", 100"}
                                        d="M18 2.0845
                                a 15.9155 15.9155 0 0 1 0 31.831
                                a 15.9155 15.9155 0 0 1 0 -31.831"
                                    />
                                    <text x="18" y="20.35" class="percentage">{avgGrade}</text>
                                </svg>
                            </div>

                        </div>
                    </div>

                    <div class="content">
                        <div class="top-content">
                            {this.props.product.description}
                            <div class="tags">
                                {
                                    this.props.product.tags.map(tag => {
                                        return (
                                            <span class="tag is-light">{tag}</span>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="is-flex is-justify-content-space-between">
                            <div class="price has-text-weight-bold">
                                <span>{this.props.product.price} <i class="fas fa-euro-sign"></i></span>
                            </div>
                            <div>
                                <button onClick={() => this.props.addToCart(this.props.product.id)} class="button is-black"><i class="fas fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        );
    }

}