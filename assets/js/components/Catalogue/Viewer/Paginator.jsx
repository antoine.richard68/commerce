import { h, Component, Fragment } from "preact";

export default class Paginator extends Component {
        
    onClick(n) {
        n = Number(n);
        if(n < 1 || n > this.props.total) return;
        this.props.onChange(n);
    }

    manageLinks() {
        let current = this.props.current,
            last = this.props.total,
            delta = 2,
            left = current - delta,
            right = current + delta + 1,
            range = [],
            links = [],
            l;

        for (let i = 1; i <= last; i++) {
            if (i == 1 || i == last || i >= left && i < right) {
                range.push(i);
            }
        }

        for (let i of range) {
            if (l) {
                if (i - l === 2) {
                    links.push((<li><a class="pagination-link" aria-label={"Goto page " + (l+1)} onClick={() => this.onClick((l+1))}>{(l+1)}</a></li>));
                } else if (i - l !== 1) {
                    links.push((<li><span class="pagination-ellipsis">&hellip;</span></li>));
                }
            }
            if(i === current) {
                links.push((<li><a class="pagination-link is-current" aria-label={"Page "+i} aria-current="page">{i}</a></li>));
            } else {
                links.push((<li><a class="pagination-link" aria-label={"Goto page " + i} onClick={() => this.onClick(i)}>{i}</a></li>));
            }
            l = i;
        }
        return links;
    }

    render() {
        return (
            <nav class="pagination is-centered" role="navigation" aria-label="pagination">
                <a class="pagination-previous" 
                    onClick={() => this.onClick(Number(this.props.current) - 1)}
                    disabled={this.props.current === 1}>Précédent</a>
                <a class="pagination-next" 
                    onClick={() => this.onClick(Number(this.props.current) + 1)}
                    disabled={this.props.current === this.props.total}>Suivant</a>
                <ul class="pagination-list">
                    {
                       this.manageLinks()
                    }
                </ul>
            </nav>
        );
    }
}