import { h, Component, Fragment, createRef } from "preact";

export default class Loader extends Component {
    

    render() {
        return(
            <div class="loader-overlay">
                <div class="overlay__inner">
                    <div class="overlay__content">
                        <span class="spinner"></span>
                        <span class="loader-text">{this.props.text}</span>
                    </div>
                </div>
            </div>  
        );
    }
}