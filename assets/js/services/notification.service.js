import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

const notificationSubject = new Subject();
const defaultId = 'default-notification';

export const notificationService = {
    onNotification,
    success,
    error,
    info,
    warn,
    notification,
    clear
};

export const notificationType = {
    success: 'success',
    error: 'error',
    info: 'info',
    warning: 'warning'
}

// enable subscribing to notifications observable
function onNotification(id = defaultId) {
    return notificationSubject.asObservable().pipe(filter(x => x && x.id === id));
}

// convenience methods
function success(message, options) {
    notification({ ...options, type: notificationType.success, message });
}

function error(message, options) {
    notification({ ...options, type: notificationType.error, message });
}

function info(message, options) {
    notification({ ...options, type: notificationType.info, message });
}

function warn(message, options) {
    notification({ ...options, type: notificationType.warning, message });
}

// core notification method
function notification(notification) {
    notification.id = notification.id || defaultId;
    notificationSubject.next(notification);
}

// clear notifications
function clear(id = defaultId) {
    notificationSubject.next({ id });
}