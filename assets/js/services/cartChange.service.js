import { Subject } from 'rxjs';

const cartChangeSubject = new Subject();


export const cartChangeService = {
    onCartChange,
    add,
    remove,
    cartChange,
};

export const cartChangeType = {
    add: 'add',
    remove: 'remove',
}

function onCartChange() {
    return cartChangeSubject.asObservable();
}

function add() {
    cartChange({type: cartChangeType.add});
}

function remove() {
    cartChange({type: cartChangeType.error});
}

function cartChange(cartChange) {
    cartChangeSubject.next(cartChange);
}
